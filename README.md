# TLSDemoCLI

TLSDemoCLi is a .NET Core console application which aims to demonstrate how HTTP traffic can be made more secure. The goal is to progress from HTTP through to HTTPS as well as demonstrating how public key pinning can be used to prevent a Man In The Middle attack.

## Pre-requisites

### .NET Core 2.2 SDK

You will need .NET Core SDK 2.2 or later to build and execute the demo application. Please download the version most appropriate for your OS here: https://dotnet.microsoft.com/download

If you are on Windows and wish to use Visual Studio 2017, there is currently a separate SDK download. If you won't be using VS2017 but perhaps Visual Studio Code, then you can download any SDK.

Once installed, confirm that it is available on your path by running `dotnet` from a command/bash prompt. You may have to log out/in or restart your machine for changes to take effect:

```
$ dotnet                                                                                                                                                                                                                                                                                                

Usage: dotnet [options]
Usage: dotnet [path-to-application]

Options:
  -h|--help         Display help.
  --info            Display .NET Core information.
  --list-sdks       Display the installed SDKs.
  --list-runtimes   Display the installed runtimes.

path-to-application:
  The path to an application .dll file to execute.
```

### An IDE or Text Editor

We will be writing a few lines of code. You can do this in a simple text editor or IDE, up to you. We will be writing less than 30 lines of code.

## Running the application

Once you have cloned this repo, navigate to the repo directory and then issue the `dotnet build` command. It may throw up a few warnings around async methods lacking "await" operators - this is OK for now.

```
$ dotnet build

Welcome to .NET Core!
---------------------
Learn more about .NET Core: https://aka.ms/dotnet-docs
Use 'dotnet --help' to see available commands or visit: https://aka.ms/dotnet-cli-docs

Telemetry
---------
The .NET Core tools collect usage data in order to help us improve your experience. The data is anonymous and doesn't include command-line arguments. The data is collected by Microsoft and shared with the community. You can opt-out of telemetry by setting the DOTNET_CLI_TELEMETRY_OPTOUT environment variable to '1' or 'true' using your favorite shell.

Read more about .NET Core CLI Tools telemetry: https://aka.ms/dotnet-cli-telemetry

ASP.NET Core
------------
Successfully installed the ASP.NET Core HTTPS Development Certificate.
To trust the certificate run 'dotnet dev-certs https --trust' (Windows and macOS only). For establishing trust on other platforms refer to the platform specific documentation.
For more information on configuring HTTPS see https://go.microsoft.com/fwlink/?linkid=848054.
Microsoft (R) Build Engine version 16.2.32702+c4012a063 for .NET Core
Copyright (C) Microsoft Corporation. All rights reserved.

  Restore completed in 2,89 sec for C:\Users\joshua.willmot\Documents\Projects\Git\TLSDemoCLI\TLSDemoCLI.csproj.
Program.cs(68,35): warning CS1998: This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread. [C:\Users\joshua.willmot\Documents\Projects\Git\TLSDemoCLI\TLSDemoCLI.csproj]
  TLSDemoCLI -> C:\Users\joshua.willmot\Documents\Projects\Git\TLSDemoCLI\bin\Debug\netcoreapp2.2\TLSDemoCLI.dll

Build succeeded.

Program.cs(68,35): warning CS1998: This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread. [C:\Users\joshua.willmot\Documents\Projects\Git\TLSDemoCLI\TLSDemoCLI.csproj]
    1 Warning(s)
    0 Error(s)

Time Elapsed 00:00:08.25
```

Once you have a successful build you can try run the application without any arguments. You should see output as below:

```
$ dotnet run
TLSDemoCLI 1.0.0
Copyright (C) 2019 TLSDemoCLI

  -l, --login-url      Specify the login URL.

  -u, --username       Specify the login username.

  -p, --password       Specify the login password.

  -c, --certificate    Path to a X509 certificate.

  --help               Display this help screen.

  --version            Display version information.
```

If you see that, you should be good to go!
