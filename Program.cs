﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace TLSDemoCLI
{
    public class Options
    {
        [Option('l', "login-url", Required = false, HelpText = "Specify the login URL.")]
        public string URL { get; set; }

        [Option('u', "username", Required = false, HelpText = "Specify the login username.")]
        public string Username { get; set; }

        [Option('p', "password", Required = false, HelpText = "Specify the login password.")]
        public string Password { get; set; }

        [Option('c', "certificate", Required = false, HelpText = "Path to a X509 certificate.")]
        public string Certificate { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var result = Parser.Default.ParseArguments<Options>(args);
            result.WithParsed<Options>(options =>
            {
                if (options.Certificate != null)
                {
                    Console.WriteLine(options);
                    if (options.URL != null || options.Username != null || options.Password != null)
                    {
                        Console.WriteLine(
                            "When using the -c/--certificate option no other arguments should be provided.\n");
                        PrintUsage(result);
                    }
                    else
                    {
                        PrintCertInfo(options.Certificate);
                    }
                }
                else if (options.URL != null && options.Username != null && options.Password != null)
                {
                    ExecuteLoginRequest(options.URL, options.Username, options.Password).Wait();
                }
                else
                {
                    PrintUsage(result);
                }
            });
        }

        private static void PrintUsage(ParserResult<Options> parserResult)
        {
            Console.WriteLine(HelpText.AutoBuild(parserResult, null, null));
        }

        private static void PrintCertInfo(string certificatePath)
        {
            X509Certificate2 cert = new X509Certificate2(certificatePath);
            string certInfo = cert.ToString(true);
            Console.WriteLine(certInfo);
        }

        private static async Task ExecuteLoginRequest(string url, string username, string password)
        {
            // Make a login request and handle the response...
            throw new NotImplementedException("Work in progress...");
        }
    }
}